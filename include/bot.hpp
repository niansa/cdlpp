#ifndef _BOT_HPP
#define _BOT_HPP
#include <string>
#include <vector>
#include <functional>
#include <nlohmann/json.hpp>
#include <boost/asio.hpp>
#include <memory>
#include "cdltypes-incomplete.hpp"
#include "dpp-wrap.hpp"

#define cdl_register_nonstatic_command(name, function, maxargs) CDL::register_command(name, [this] (CMessage msg, CChannel channel, CDL::cmdargs& args) { (function)(msg, channel, args); }, maxargs)
constexpr auto NO_ARGS = 1;
constexpr auto INF_ARGS = 0;


namespace CDL {
using json = nlohmann::json;

namespace handlers {
    extern std::function<void (const std::string& message)> on_error;
    extern std::function<void ()> in_main;
    extern std::function<void (CMessage msg, std::function<void (CMessage)> cb)> on_message;
    extern std::function<void (CChannel channel, std::function<void (const std::string&)> cb)> get_prefix;
}

namespace asio = boost::asio;

using cmdargs = std::vector<std::string>;
using cmdfnc = std::function<void (CMessage, CChannel, cmdargs&)>;
namespace intent_vals = discordpp::intents;

struct cmd {
    cmdfnc function;
    cmdargs::size_type maxargs;
};

extern std::unordered_map<std::string, cmd> commands;


[[ noreturn ]] void restart_bot();
void reload_config();
void register_command(const std::string& name, cmdfnc, cmdargs::size_type maxargs);
void deregister_commands(const std::vector<std::string>& names);
void register_module_initialiser(std::function<void ()>);
void main(int argc, char **argv, uint16_t intents = discordpp::intents::NONE, std::shared_ptr<boost::asio::io_context> aioc = nullptr);
};
#endif
