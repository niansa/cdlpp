#ifndef _EXTRAS_HPP
#define _EXTRAS_HPP
#include <string>
#include <string_view>
#include <vector>
#include "bot.hpp"
#include "cdltypes-incomplete.hpp"

#ifdef __linux__
#    define extras_get_mem_supported
#endif

namespace CDL {
namespace extras {
    std::vector<std::string> strsplit(std::string_view s, char delimiter, std::vector<std::string>::size_type times = 0);
    std::string get_premention(const_CChannel channel, const_CUser author, std::string message);
    std::string get_avatar_url(const_CUser);
    std::string get_avatar_url(const_CGuild);
    bool is_digits(std::string str, bool maybe_negative = false);
    std::string tolowers(std::string str);
    std::string touppers(std::string str);
    std::string url_encode(const std::string &str);
    uint64_t find_user_id(CGuild guild, std::string identification);
    const std::string &get_bot_invite_link();
    namespace get_mem {
        size_t used();
        size_t total();
    }
}
}
#endif
