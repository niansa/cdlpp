#include "basemessage.hpp"
#include "channel.hpp"
#include "member.hpp"
#include "reaction.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _MESSAGE_HPP
#define _MESSAGE_HPP
namespace CDL {
struct Mention;
struct BaseMessage;
class Message;
}

#include <string>
#include <vector>
#include <functional>
#include <nlohmann/json.hpp>



namespace CDL {
class Message : public std::enable_shared_from_this<Message>, public BaseMessage {
public:
    uint64_t id,
             channel_id,
             guild_id = 0,
             webhook_id = 0;
    bool webhook;
    CUser author;
    CMember member = nullptr;
    std::vector<CUser> mentions;
    std::vector<uint64_t> mention_roles,
                          mention_channels;
    std::vector<Reaction> reactions;

    Message(const nlohmann::json& data) {
        update(data);
    }
    void get_channel(std::function<void (CChannel)> cb) const {
        fetch::channel(channel_id, cb);
    }
    void get_guild(std::function<void (CGuild)> cb) const {
        if (not guild_id) {
            cb(nullptr);
        } else {
            fetch::guild(guild_id, cb);
        }
    }
    CGuild get_guild() const {
        if (not member) {
            return nullptr;
        } else {
            return member->guild;
        }
    }
    template<typename... Args>
    void reply(Args&&...args) {
        auto sharedthis = shared_from_this();
        get_channel([sharedthis, args...] (CChannel channel) {
            channel->_in_reply_to = sharedthis;
            channel->send(args...);
            channel->_in_reply_to = nullptr;
        });
    }

    void commit(std::function<void (const bool)> cb = nullptr, CMessage source = nullptr);
    void update(const nlohmann::json& data);
    void edit(const std::string& text, std::function<void (CMessage)> cb = nullptr, const nlohmann::json& embed = nullptr, uint32_t flags = 0);
    void remove(std::function<void (const bool)> cb = nullptr);
    void pin(std::function<void (const bool)> cb = nullptr);
    void unpin(std::function<void (const bool)> cb = nullptr);
    void add_reaction(CEmoji emoji, std::function<void (const bool)> cb = nullptr);
};
}
#endif
