#include "../cdltypes-incomplete.hpp"

#ifndef _PRESENCE_HPP
#define _PRESENCE_HPP
namespace CDL {
class PresenceUpdate;
class Activity;
}

#include <string>
#include <vector>
#include <functional>
#include <initializer_list>
#include <nlohmann/json.hpp>
#include <boost/asio.hpp>



namespace CDL {
// https://discord.com/developers/docs/topics/gateway#update-status-status-types
namespace PresenceStatus {
extern std::initializer_list<std::string> strings;
enum type {
    online,
    dnd,
    idle,
    invisible,
    offline
};
}

// https://discord.com/developers/docs/topics/gateway#client-status-object
namespace ClientStatus {
enum type {
    desktop,
    mobile,
    web,
    bot = web
};
}

// https://discord.com/developers/docs/topics/gateway#activity-object-activity-types
namespace ActivityType {
enum type {
    playing,
    streaming,
    listening,
    watching,
    custom,
    none
};
}

// https://discord.com/developers/docs/topics/gateway#activity-object
class Activity {
public:
    std::string text;
    ActivityType::type type = ActivityType::none;
    std::string url;

    Activity(const std::string& text, ActivityType::type type, const std::string& url = "") : text(text), type(type), url(url) {}
    Activity(const nlohmann::json& data) {
        update(data);
    }

    void update(const nlohmann::json& data);
    nlohmann::json dump() const;
};

// https://discord.com/developers/docs/topics/gateway#update-status-gateway-status-update-structure
class Presence {
public:
    int32_t idle_since = 0;
    PresenceStatus::type status = PresenceStatus::online;
    std::vector<Activity> activities;
    ClientStatus::type clientStatus = ClientStatus::bot;
    CUser user = nullptr;

    Presence() {}
    Presence(const nlohmann::json& data) {
        update(data);
    }

    void update(const nlohmann::json& data);
    nlohmann::json dump() const;
    void commit();
};

class PresenceScroller {
    size_t presence_selector = 0;

    boost::asio::steady_timer *timer = nullptr;
    void doScroll();

public:
    std::vector<std::function<Presence ()>> presences;
    time_t speed = 15;

    PresenceScroller() {
        doScroll();
    }
};
}
#endif
