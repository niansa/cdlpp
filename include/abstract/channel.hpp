#include "basechannel.hpp"
#include "basemessage.hpp"
#include "user.hpp"
#include "role.hpp"
#include "channel_types.hpp"
#include "permissions.hpp"
#include "fetch.hpp"
#include "cache.hpp"
#include "env.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _CHANNEL_HPP
#define _CHANNEL_HPP
namespace CDL {
struct BaseChannel;
class Channel;
}

#include <string>
#include <vector>
#include <optional>
#include <nlohmann/json.hpp>



namespace CDL {
// https://discord.com/developers/docs/resources/voice#voice-state-object
class VoiceStateUpdate {
public:
    uint64_t guild_id = 0,
             channel_id,
             user_id;
    CMember member;
    std::string session_id;
    bool mute,
         deaf,
         self_mute,
         self_deaf,
         self_stream = false,
         self_video;

    VoiceStateUpdate(const nlohmann::json& data);
};

class Channel: public BaseChannel {
    std::unordered_map<uint64_t, CMessage> *_pins_cache = nullptr;
public:
    uint64_t id,
             guild_id = 0,
             last_message_id = 0;
    std::unordered_map<uint64_t, CMessage> messages;
    CMessage _in_reply_to = 0;

    Channel(const nlohmann::json& data) {
        update(data);
    }
    ~Channel() {
        if (_pins_cache) delete _pins_cache;
    }
    CGuild get_guild() const {
        // It's usually safe to use the cache - having cached the channel would mean having cached the guild too
        // The only exception is not having enabled the GUILDS intent
        return cache::get_guild(guild_id);
    }
    void get_guild(std::function<void (CGuild)> cb) const {
        if (not guild_id) {
            cb(nullptr);
        } else {
            fetch::guild(guild_id, cb);
        }
    }
    void send(BaseMessage& msg, std::function<void (CMessage)> cb = nullptr) {
        send_json(msg.dump(), cb);
    }
    void send(const std::string& text, std::function<void (CMessage)> cb = nullptr, const nlohmann::json& embed = nullptr) {
        send_json(nlohmann::json({
                 {"content", text},
                 {"embed", embed}
             }), cb);
    }
    void send_embed(const nlohmann::json& embed, const std::string& text = "", std::function<void (CMessage)> cb = nullptr) {
        send(text, cb, embed);
    }
    bool has_perm(CUser user, Permissions::type permission) const {
        return has_perm(user->id, false, permission);
    }
    bool has_perm(Role *role, Permissions::type permission) const {
        return has_perm(role->id, true, permission);
    }

    void delete_message(uint64_t msg_id, std::function<void (const bool)> cb) {
        env.bot->call("DELETE", "/channels/" + std::to_string(id) + "/messages/" + std::to_string(msg_id), cb);
    }
    void delete_message(uint64_t msg_id) {
        env.bot->call("DELETE", "/channels/" + std::to_string(id) + "/messages/" + std::to_string(msg_id));
    }
    void _pins_cache_invalidate() {
        if (_pins_cache) {
            delete _pins_cache;
            _pins_cache = nullptr;
        }
    }

    void update(const nlohmann::json& data);
    void commit(std::function<void (const bool)> cb = nullptr, CChannel source = nullptr);
    __attribute__((pure)) std::string get_mention() const;
    void send_json(nlohmann::json msg, std::function<void (CMessage)> cb = nullptr);
    void edit_message(const uint64_t message_id, const std::string& text, std::function<void (CMessage)> cb = nullptr, const nlohmann::json& embed = nullptr, uint32_t flags = 0);
    bool has_perm(uint64_t uor_id, bool is_role, Permissions::type permission) const;
    void remove(std::function<void (const bool)> cb = nullptr);
    void get_pins(std::function<void (const bool, const std::unordered_map<uint64_t, CMessage>&)> cb);
    void pin_message(uint64_t msg_id, std::function<void (const bool)> cb = nullptr);
    void unpin_message(uint64_t msg_id, std::function<void (const bool)> cb = nullptr);
    void pin_message(CMessage msg, std::function<void (const bool)> cb = nullptr);
    void unpin_message(CMessage msg, std::function<void (const bool)> cb = nullptr);
    void create_invite(std::function<void (std::optional<Invite>)> cb = nullptr, bool temporary = false, bool unique = true, uint32_t max_uses = 0, uint32_t max_age = 0);
    void get_invites(std::function<void (std::optional<std::vector<Invite>>)> cb);
    void start_typing(std::function<void (const bool)> cb = nullptr);
};
}
#endif
