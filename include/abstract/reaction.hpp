#include "fetch.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _REACTION_HPP
#define _REACTION_HPP
namespace CDL {
class Reaction;
}

#include <string>
#include <nlohmann/json.hpp>



namespace CDL {
// https://discord.com/developers/docs/topics/gateway#message-reaction-add-message-reaction-add-event-fields
class Reaction {
    nlohmann::json _emoji_raw;
    CEmoji _emoji = nullptr;

public:
    uint64_t user_id,
             channel_id,
             message_id,
             guild_id = 0;
    CMember member = nullptr;

    bool operator ==(const Reaction& r) {
        if (message_id != r.message_id) return false;
        if (member != r.member) return false;
        return true;
    }
    void get_user(std::function<void (CUser)> cb) const {
        fetch::user(user_id, cb);
    }
    void get_channel(std::function<void (CChannel)> cb) const {
        fetch::channel(channel_id, cb);
    }
    void get_guild(std::function<void (CGuild)> cb) const {
        fetch::guild(guild_id, cb);
    }

    Reaction(const nlohmann::json& data);
    void get_emoji(std::function<void (CEmoji)> cb);
    void get_emoji(std::function<void (CEmoji)> cb) const;
};
}
#endif
