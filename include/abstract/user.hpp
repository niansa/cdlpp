#include "presence.hpp"
#include "../extras.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _USER_HPP
#define _USER_HPP
namespace CDL {
class User;
}

#include <string>
#include <nlohmann/json.hpp>



namespace CDL {
// https://discord.com/developers/docs/resources/user#user-object
class User : public std::enable_shared_from_this<User> {
public:
    uint64_t id;
    std::string username,
                avatar;
    bool system = false,
         bot = false,
         mfa_enabled = false;
    int discriminator;
    Presence *presence = nullptr;

    User(const nlohmann::json& data) {
        update(data);
    }
    ~User() {
        if (presence) delete presence;
    }
    std::string get_full_name() const {
        return username + '#' + std::to_string(discriminator);
    }

    std::string get_mention() const {
        return "<@" + std::to_string(id) + ">";
    }

    std::string get_avatar_url() {
        return extras::get_avatar_url(shared_from_this());
    }

    nlohmann::json dump();
    void update(const nlohmann::json& data);
    CMember get_member(CGuild guild) const;
    CMember get_member(uint64_t guild_id) const;
    void get_dm(std::function<void (CChannel )> cb);
};
}
#endif
