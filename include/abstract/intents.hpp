#include <unordered_map>
#include <functional>
#include "presence.hpp"
#include "reaction.hpp"
#include "../cdltypes-incomplete.hpp"


namespace CDL {
namespace intents {
    using readyHandler = std::function<void ()>;

    using memberHandler = std::function<void (CMember)>;
    using memberHandler_r = std::function<void (const_CMember)>;

    using roleHandler = std::function<void (Role*)>;
    using roleHandler_r = std::function<void (const Role*)>;

    using messageHandler = std::function<void (CMessage)>;
    using messageHandler_r = std::function<void (const_CMessage)>;

    using channelHandler = std::function<void (CChannel)>;
    using channelHandler_r = std::function<void (const_CChannel)>;

    using emojiHandler = std::function<void (CGuild)>;

    using banHandler = std::function<void (CGuild, Ban&)>;
    using banHandler_r = std::function<void (CGuild, const Ban&)>;

    using guildHandler = std::function<void (CGuild)>;
    using guildHandler_r = std::function<void (const_CGuild)>;

    using presenceHandler = std::function<void (Presence*)>;

    using reactionHandler = std::function<void (Reaction&)>;
    using reactionHandler_r = std::function<void (const Reaction&)>;

    using voiceUpdateHandler = std::function<void (const VoiceStateUpdate&)>;


    extern std::vector<readyHandler> ready;
    extern std::vector<memberHandler> guild_member_add,
                                      guild_member_update;
    extern std::vector<memberHandler_r> guild_member_remove;
    extern std::vector<roleHandler> guild_role_create,
                                    guild_role_update;
    extern std::vector<roleHandler_r> guild_role_delete;
    extern std::vector<messageHandler> message_create,
                                       message_update,
                                       message_remove_all_reactions;
    extern std::vector<messageHandler_r> message_delete;
    extern std::vector<channelHandler> channel_create,
                                       channel_update,
                                       channel_pins_update;
    extern std::vector<channelHandler_r> channel_delete;
    extern std::vector<emojiHandler> guild_emojis_update;
    extern std::vector<banHandler> guild_ban_add;
    extern std::vector<banHandler_r> guild_ban_remove;
    extern std::vector<guildHandler> guild_create,
                                     guild_update,
                                     guild_join;
    extern std::vector<guildHandler_r> guild_delete;
    extern std::vector<presenceHandler> presence_update;
    extern std::vector<reactionHandler> message_reaction_add;
    extern std::vector<reactionHandler_r> message_reaction_remove;
    extern std::vector<voiceUpdateHandler> voice_state_update;

    void init();
}
}
