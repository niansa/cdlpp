#include "permissions.hpp"
#include "cache.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _ROLE_HPP
#define _ROLE_HPP
namespace CDL {
struct RoleTags;
class Role;
}

#include <cstdint>
#include <string>
#include <optional>
#include <nlohmann/json.hpp>



namespace CDL {
// https://discord.com/developers/docs/topics/permissions#role-object-role-tags-structure
struct RoleTags {
    uint64_t bot_id = 0;
    uint64_t integration_id = 0;
    bool premium_subscriber = false;
};

// https://discord.com/developers/docs/topics/permissions#role-object
struct RoleUpdate {
    std::optional<Permissions::type> permissions;
    std::optional<std::string> name;
    std::optional<uint32_t> color;
    std::optional<bool> hoist,
                        mentionable;
    std::optional<int> position;

    nlohmann::json dump() const;
};

class Role {
public:
    uint64_t id,
             guild_id;
    std::string name = "new role";
    uint32_t color = 0;
    int32_t position;
    Permissions::type permissions = Permissions::NONE;
    bool hoist = false,
         managed,
         mentionable = false;
    RoleTags tags;
    CGuild lazy_guild = nullptr;

    Role(const nlohmann::json& data, uint64_t guild_id) {
        update(data, guild_id);
    }
    Role(const nlohmann::json& data, CGuild guild) {
        update(data, guild);
    }
    void update(const nlohmann::json& data, uint64_t guild_id) {
        update(data, cache::get_guild(guild_id));
        this->guild_id = guild_id;
    }
    std::string get_mention() const {
        return "<@&" + std::to_string(id) + ">";
    }

    void update(const nlohmann::json& data, CGuild guild);
    nlohmann::json dump(bool position = false) const;
    void commit(std::function<void (const bool)> cb = nullptr, const RoleUpdate& source = {});
    void commit(std::function<void (const bool)> cb = nullptr, const Role *source = nullptr);
    void get_guild(std::function<void (CGuild)> cb);
    void get_guild(std::function<void (CGuild)> cb) const;
    void remove(std::function<void (const bool)> cb);
};
}
#endif
