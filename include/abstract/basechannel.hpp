#include "channel_types.hpp"

#ifndef _BASECHANNEL_HPP
#define _BASECHANNEL_HPP
#include <string>
#include <unordered_map>
#include <nlohmann/json.hpp>



namespace CDL {

// https://discord.com/developers/docs/resources/channel#overwrite-object
struct PermissionOverwrite {
    uint64_t id; // role or user id
    enum Types {
        role,
        member
    };
    bool type; // ether 0 (role) or 1 (member)
    // Are we wasting 32 bits?
    uint64_t allow;
    uint64_t deny;
};

// https://discord.com/developers/docs/resources/channel#channel-object
struct BaseChannel {
    std::string name,
                topic;
    ChannelTypes::type type = ChannelTypes::GUILD_TEXT;
    uint64_t parent_id = 0;
    int32_t position = 0,
            user_limit = 0,
            rate_limit_per_user = 0,
            bitrate = 64000;
    bool nsfw = false;
    std::unordered_map<uint64_t, PermissionOverwrite> overwrites;

    nlohmann::json dump() const;
};
}
#endif
