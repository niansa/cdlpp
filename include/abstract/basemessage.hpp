#ifndef _BASEMESSAGE_HPP
#define _BASEMESSAGE_HPP
#include <string>
#include <nlohmann/json.hpp>



namespace CDL {
// https://discord.com/developers/docs/resources/channel#message-object
struct BaseMessage {
    std::string content;
    nlohmann::json embed;

    nlohmann::json dump() const;
};
}
#endif
