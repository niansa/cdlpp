#include "../cdltypes-incomplete.hpp"

#ifndef _CACHE_HPP
#define _CACHE_HPP
#include <memory>
#include <nlohmann/json.hpp>



namespace CDL {
namespace cache {
using json = nlohmann::json;

extern std::unordered_map<uint64_t, CChannel> channel_cache;
extern std::unordered_map<uint64_t, CUser> user_cache;
extern std::unordered_map<uint64_t, CEmoji> emoji_cache;
extern std::unordered_map<uint64_t, CGuild> guild_cache;

// User cache
CUser get_user(uint64_t user_id);
CUser new_user(const nlohmann::json& data);

// Emoji cache
CEmoji get_emoji(uint64_t emoji_id);
CEmoji new_emoji(const nlohmann::json& data, Guild *guild);
void remove_emoji(uint64_t emoji_id);

// Guild/member/channel cache
CGuild new_guild(const nlohmann::json& data);
CGuild get_guild(uint64_t guild_id);
CMember new_member(const nlohmann::json& data, uint64_t guild_id, uint64_t user_id, CUser user = nullptr);
void remove_member(uint64_t guild_id, uint64_t user_id);

// Channel/message cache
CChannel new_channel(const nlohmann::json& data, uint64_t guild_id = 0);
CChannel get_channel(uint64_t channel_id);
CMessage new_message(const nlohmann::json& data);

// Exceptions
struct guildOfMemberNotCachedError : std::exception {
    const char* what() const noexcept;
};
struct guildOfChannelNotCachedError : std::exception {
    const char* what() const noexcept;
};
struct channelOfMessageNotCachedError : std::exception {
    const char* what() const noexcept;
};
struct guildChannelRequiresGuildIdError : std::exception {
    const char* what() const noexcept;
};

// Cache control
namespace ctrl {
    extern bool user,
                emoji,
                guild,
                member,
                channel,
                message;
}
}
}
#endif
