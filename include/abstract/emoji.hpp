#include "../extras.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _EMOJI_HPP
#define _EMOJI_HPP
namespace CDL {
struct Emoji;
}

#include <string>
#include <nlohmann/json.hpp>



namespace CDL {
// https://discord.com/developers/docs/resources/emoji
class Emoji {
public:
    uint64_t id;
    std::string name;
    std::vector<Role*> roles;
    CUser user = nullptr;
    Guild *guild;
    bool require_colons = true,
         managed = false,
         animated = false,
         available = true,
         custom;

    Emoji(const nlohmann::json& data, Guild *guild) {
        update(data, guild);
    }
    std::string get_url() const {
        return "https://cdn.discordapp.com/emojis/"+std::to_string(id)+".png";
    }
    std::string get_mention() const {
        if (custom) return (animated?"<a:":"<:")+name+":"+std::to_string(id)+">";
        else return name;
    }
    std::string url_encode() const {
        return custom?(extras::url_encode(name)+"%3A"+std::to_string(id)):extras::url_encode(name);
    }

    void update(const nlohmann::json& data, Guild *guild = nullptr);
};
}
#endif
