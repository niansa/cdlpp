#include "user.hpp"
#include "channel.hpp"
#include "permissions.hpp"
#include "fetch.hpp"
#include "role.hpp"
#include "../extras.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _GUILD_HPP
#define _GUILD_HPP
namespace CDL {
class Ban;
class Feature;
class Guild;
}

#include <string>
#include <map>
#include <unordered_map>
#include <nlohmann/json.hpp>



namespace CDL {
namespace specialChannel {
    enum type {
        afk,
        widget,
        system,
        rules,
        public_updates
    };
}

// https://discord.com/developers/docs/resources/guild#guild-object-default-message-notification-level
namespace notificationLevel {
    enum type {
        all_messages,
        only_mentions
    };
}

// https://discord.com/developers/docs/resources/guild#guild-object-explicit-content-filter-level
namespace exciplitContentFilterLevel {
    enum type {
        disabled,
        members_without_roles,
        all_members
    };
}

// https://discord.com/developers/docs/resources/guild#guild-object-mfa-level
namespace mfaLevel {
    enum type {
        none,
        elevated
    };
}

// https://discord.com/developers/docs/resources/guild#guild-object-verification-level
namespace verificationLevel {
    enum type {
        none,
        low,
        medium,
        high,
        very_high
    };
}

// https://discord.com/developers/docs/resources/invite#invite-object-target-user-types
namespace targetUser {
    enum type {
        neutral,
        stream
    };
}

// https://discord.com/developers/docs/resources/guild#guild-object-guild-features
std::string feature_get_nice(const std::string& feature_str);

// https://discord.com/developers/docs/resources/guild#ban-object
class Ban {
public:
    CUser user = nullptr;
    std::string reason;

    Ban(const nlohmann::json& data);
};

// https://discord.com/developers/docs/resources/invite#invite-object
class Invite {
public:
    std::string code;
    CGuild guild = nullptr;
    CChannel channel;
    CUser inviter = nullptr;
    CUser target_user = nullptr;
    targetUser::type target_user_type = targetUser::neutral;

    Invite(const nlohmann::json& data);
};

// https://discord.com/developers/docs/resources/channel#channel-object
class Guild : public std::enable_shared_from_this<Guild> {
    void move_what(uint64_t obj_id, const std::string& ep, int32_t position, std::function<void (const bool error)> cb = nullptr);
    void remove_what(uint64_t obj_id, const std::string& ep, std::function<void (const bool error)> cb = nullptr);
public:
    std::string name,
                region,
                icon_hash,
                splash_hash,
                discovery_splash_hash,
                my_permissions,
                vanity_url_code,
                description,
                banner_hash,
                preferred_locale;
    uint64_t id,
             owner_id,
             application_id = 0,
             afk_channel_id = 0,
             widget_channel_id = 0,
             system_channel_id = 0,
             rules_channel_id = 0,
             public_updates_channel_id = 0;
    uint32_t premium_subscription_count = 0,
             afk_timeout,
             system_channel_flags,
             member_count = 0,
             max_presences = 25000,
             max_members = 0,
             max_video_channel_users = 0,
             approximate_member_count = 0,
             approximate_presence_count = 0;
    bool is_owner,
         widget_enabled = false,
         large = false,
         unavailable = false;
    uint16_t premium_tier;
    notificationLevel::type default_message_notifications;
    exciplitContentFilterLevel::type explicit_content_filter;
    mfaLevel::type mfa_level;
    verificationLevel::type verification_level;
    std::unordered_map<uint64_t, CMember> members;
    std::unordered_map<uint64_t, CChannel> channels;
    std::unordered_map<uint64_t, CEmoji> emojis;
    std::map<uint64_t, Role*> roles;
    std::vector<std::string> features;

    std::unordered_map<uint64_t, Ban*> _bans_cache;
    bool _bans_cache_use = false;

    static void create(const nlohmann::json& src, std::function<void (CGuild)> cb = nullptr);
    static void create(Guild& src, std::function<void (CGuild)> cb = nullptr) {
        create(src.dump(), cb);
    }
    void get_owner(std::function<void (CUser)> cb) {
        fetch::user(owner_id, cb);
    }
    auto get_icon_url() const {
        return extras::get_avatar_url(shared_from_this());
    }
    auto has_perm(CUser user, Permissions::type permission) const {
        return has_perm(user->id, false, permission);
    }
    auto has_perm(Role *role, Permissions::type permission) const {
        return has_perm(role->id, true, permission);
    }
    void create_channel(BaseChannel& channel, std::function<void (CChannel)> cb) {
        create_channel(channel.dump(), cb);
    }
    void move_channel(uint64_t channel_id, int32_t position, std::function<void (const bool error)> cb = nullptr) {
        move_what(channel_id, "/channels", position, cb);
    }
    void move_channel(CChannel channel, int32_t position, std::function<void (const bool error)> cb = nullptr) {
        move_channel(channel->id, position, cb);
    }

    void move_role(uint64_t role_id, int32_t position, std::function<void (const bool error)> cb = nullptr) {
        move_what(role_id, "/roles", position, cb);
    }
    void move_role(Role *role, int32_t position, std::function<void (const bool error)> cb = nullptr) {
        move_channel(role->id, position, cb);
    }

    void remove_channel(uint64_t channel_id, std::function<void (const bool error)> cb = nullptr) {
        Channel incomplete_channel({});
        incomplete_channel.id = channel_id;
        incomplete_channel.remove(cb);
    }
    void remove_channel(CChannel channel, std::function<void (const bool error)> cb = nullptr) {
        channel->remove(cb);
    }
    void remove_role(Role *role, std::function<void (const bool error)> cb = nullptr) {
        remove_channel(role->id, cb);
    }

    Guild(const nlohmann::json& data);
    ~Guild();
    void update(const nlohmann::json& data);
    nlohmann::json dump() const;
    void commit(std::function<void (const bool)> cb = nullptr, CGuild source = nullptr);
    void remove(std::function<void (const bool)> cb = nullptr);
    void clear_emoji_cache();
    bool has_perm(uint64_t uor_id, bool is_role, Permissions::type permission) const;
    std::vector<Role*> get_roles(uint64_t user_id) const;
    std::vector<Role*> get_roles(const_CUser user) const;
    void get_bans(std::function<void (const std::unordered_map<uint64_t, Ban*>&)> cb);
    void set_nick(uint64_t user_id, const std::string& new_nick, std::function<void (const bool error)> cb = nullptr);
    void ban(uint64_t user_id, const std::string& reason = "", uint16_t delete_message_days = 0, std::function<void (const bool error)> cb = nullptr);
    void ban(CUser user, const std::string& reason = "", uint16_t delete_message_days = 0, std::function<void (const bool error)> cb = nullptr);
    void kick(uint64_t user_id, const std::string& reason = "", std::function<void (const bool error)> cb = nullptr);
    void kick(CUser user, const std::string& reason = "", std::function<void (const bool error)> cb = nullptr);
    void unban(uint64_t user_id, std::function<void (const bool error)> cb = nullptr);
    void unban(CUser user, std::function<void (const bool error)> cb = nullptr);
    uint64_t get_special_channel(specialChannel::type special_channel_type) const;
    void get_special_channel(specialChannel::type special_channel_type, std::function<void (CChannel)> cb);
    void create_channel(const nlohmann::json& channel, std::function<void (CChannel)> cb = nullptr);
    void create_role(const nlohmann::json& role, std::function<void (Role*)> cb = nullptr);
    void create_role(Role *role, std::function<void (Role*)> cb = nullptr);
    void remove_role(uint64_t role_id, std::function<void (const bool error)> cb = nullptr);
    void set_nick(CMember member, const std::string& new_nick, std::function<void (const bool error)> cb = nullptr);
};
}
#endif
