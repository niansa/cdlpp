#include "../cdltypes-incomplete.hpp"

#ifndef _FETCH_HPP
#define _FETCH_HPP
#include <functional>



namespace CDL {
namespace fetch {
void message(uint64_t id, std::function<void (CMessage)> cb, const_CChannel channel);
void channel(uint64_t id, std::function<void (CChannel)> cb, const_CGuild guild = nullptr);
void guild(uint64_t id, std::function<void (CGuild)> cb);
void user(uint64_t id, std::function<void (CUser)> cb);
}
}
#endif
