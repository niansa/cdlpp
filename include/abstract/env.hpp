#include "presence.hpp"
#include "../dpp-wrap.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _ENV_HPP
#define _ENV_HPP
namespace CDL {
struct Env;
}

#include <memory>
#include <boost/asio.hpp>
#ifdef WITH_CDLPPDB
#    include "database.hpp"
#endif



namespace CDL {
struct Env {
    std::shared_ptr<Dpp> bot;
    nlohmann::json settings;
    CUser self = nullptr;
    std::shared_ptr<boost::asio::io_context> aioc = nullptr;
    Presence presence;

    int argc = 0;
    char **argv = nullptr;
    std::string prg_path;

#   ifdef WITH_CDLPPDB
    Database *db = nullptr;
#   endif
};

extern Env env;
}
#endif
