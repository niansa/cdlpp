#include "guild.hpp"
#include "role.hpp"
#include "../cdltypes-incomplete.hpp"

#ifndef _MEMBER_HPP
#define _MEMBER_HPP
namespace CDL {
class Member;
}

#include <string>
#include <vector>
#include <optional>
#include <nlohmann/json.hpp>



namespace CDL {
// https://discord.com/developers/docs/resources/guild#modify-guild-member
struct VoiceState {
    std::optional<bool> mute, deaf;
    uint64_t channel_id = 0;
};

// https://discord.com/developers/docs/resources/guild#guild-member-object
class Member {
public:
    std::string nick;
    bool deaf,
         mute;
    uint64_t user_id;
    CUser lazy_user;
    CGuild guild;
    std::vector<uint64_t> roles;

    Member(const nlohmann::json& data, uint64_t user_id, CGuild guild, CUser user = nullptr) {
        update(data, user_id, guild, user);
    }
    bool has_perm(Permissions::type permission) {
        return guild->has_perm(user_id, false, permission);
    }
    template<typename... Args>
    void kick(Args&... args) {
        guild->kick(user_id, args...);
    }
    template<typename... Args>
    void ban(Args&... args) {
        guild->ban(user_id, args...);
    }
    bool operator >(CMember other) const {
        auto myHighestRole = guild->roles[roles.front()],
             otherHighestRole = guild->roles[other->roles.front()];
        return myHighestRole->position > otherHighestRole->position;
    }

    void update(const nlohmann::json& data, uint64_t user_id, CGuild guild, CUser user = nullptr);
    nlohmann::json dump() const;
    void commit(std::function<void (const bool)> cb = nullptr, CMember source = nullptr);
    void get_user(std::function<void (CUser)> cb);
    void get_user(std::function<void (CUser)> cb) const;
    void set_nick(const std::string& new_nick, std::function<void (const bool)> cb = nullptr);
    void set_voice_state(VoiceState voiceState, std::function<void (const bool)> cb = nullptr);
};
}
#endif
