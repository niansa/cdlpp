class Dpp;



#ifndef _DPP_WRAP_HPP
#define _DPP_WRAP_HPP
#include <string>
#include <utility>
#include <boost/asio.hpp>
#include <nlohmann/json.hpp>
#ifndef _DPP_IMPORTED
#define _DPP_IMPORTED
#include "dpp/alias.hpp"
#include "dpp/intents.hpp"
#endif

namespace dpptypes = discordpp;



class Dpp {
    using json = nlohmann::json;
    void *src;

public:
    std::multimap<std::string, dpptypes::handleEvent> *handlers;
    uint16_t *intents;

    Dpp();
    ~Dpp();

    void initBot(unsigned int apiVersionIn, const std::string &tokenIn,
                 std::shared_ptr<boost::asio::io_context> aiocIn);

    void run();

    void call(const std::string &requestType,
              const std::string &targetURL);

    void call(const std::string &requestType,
              const std::string &targetURL, const json &body);

    void call(const std::string &requestType,
              const std::string &targetURL,
              const dpptypes::handleWrite &onWrite);

    void call(const std::string &requestType,
              const std::string &targetURL, const dpptypes::handleRead &onRead);

    void call(const std::string &requestType,
              const std::string &targetURL, const json &body,
              const dpptypes::handleWrite &onWrite);

    void call(const std::string &requestType,
              const std::string &targetURL, const json &body,
              const dpptypes::handleRead &onRead);

    void call(const std::string &requestType,
              const std::string &targetURL, const json &body,
              const dpptypes::handleWrite &onWrite, const dpptypes::handleRead &onRead);

    void send(const int opcode, const json &payload);
};
#endif
