#include <string>
#include <vector>
#include <nlohmann/json.hpp>
#include "abstract/channel.hpp"
#include "abstract/message.hpp"
#include "abstract/member.hpp"
#include "abstract/guild.hpp"
#include "abstract/role.hpp"
#include "abstract/json_f.hpp"
#include "abstract/env.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "cdltypes-incomplete.hpp"


namespace CDL {
VoiceStateUpdate::VoiceStateUpdate(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("guild_id", guild_id);
        JSON_FPARSE_ID("channel_id", channel_id);
        JSON_FPARSE_ID("user_id", user_id);
        JSON_FPARSE("session_id", session_id);
        JSON_FPARSE("deaf", deaf);
        JSON_FPARSE("mute", mute);
        JSON_FPARSE("self_deaf", self_deaf);
        JSON_FPARSE("self_mute", self_mute);
        JSON_FPARSE("self_stream", self_stream);
        JSON_FPARSE("self_video", self_video);
    }
    if (data.contains("member")) {
        auto guild = cache::get_guild(guild_id);
        member = std::make_shared<Member>(data["member"], user_id, guild, cache::get_user(user_id));
        if (guild) {
            guild->members[user_id] = member;
        }
    }
}

void Channel::update(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("id", id);
        JSON_FPARSE_ID("guild_id", guild_id);
        JSON_FPARSE_ID("parent_id", parent_id);
        JSON_FPARSE_ID("last_message_id", last_message_id);
        JSON_FPARSE("type", type);
        JSON_FPARSE("name", name);
        JSON_FPARSE("position", position);
        JSON_FPARSE("user_limit", user_limit);
        JSON_FPARSE("rate_limit_per_user", rate_limit_per_user);
        JSON_FPARSE("bitrate", bitrate);
        JSON_FPARSE("nsfw", nsfw);
        JSON_FPARSE_CUSTOM("permission_overwrites", obj, {
                               overwrites.clear();
                               for (const auto& [_, odata] : obj.items()) {
                                   PermissionOverwrite overwrite = {
                                       s2i(odata["id"]),
                                       odata["type"]=="member",
                                       s2i(odata["allow"]),
                                       s2i(odata["deny"])
                                   };
                                   overwrites[overwrite.id] = overwrite;
                               }
                           });
    }
}

nlohmann::json BaseChannel::dump() const {
    nlohmann::json res;

    ChannelTypes::type type_in_dump;
    switch (type) {
        using namespace ChannelTypes;
        case GUILD_CATEGORY: case GUILD_TEXT: case GUILD_VOICE: type_in_dump = type; break;
        default: type_in_dump = GUILD_TEXT;
    }

    JSON_FDUMP_BEGIN(res) {
        JSON_FDUMP_ID("parent_id", parent_id);
        JSON_FDUMP("name", name);
        JSON_FDUMP("type", type_in_dump);
        JSON_FDUMP("topic", topic);
        JSON_FDUMP("bitrate", bitrate);
        JSON_FDUMP("user_limit", user_limit);
        JSON_FDUMP("rate_limit_per_user", rate_limit_per_user);
        JSON_FDUMP("position", position);
        JSON_FDUMP("nsfw", nsfw);
        JSON_FDUMP_CUSTOM("permission_overwrites", array, jpovers, {
                              for (const auto& [pover_id, pover] : overwrites) {
                                  nlohmann::json jpover;
                                  JSON_FDUMP_BEGIN(jpover) {
                                      JSON_FDUMP_ID("id", pover.id);
                                      JSON_FDUMP("allow", std::to_string(pover.allow));
                                      JSON_FDUMP("deny", std::to_string(pover.deny));
                                      JSON_FDUMP_VFNC("type", pover.type, uint32_t);
                                  }
                                  jpovers.push_back(jpover);
                              }
                          });
    }
    return res;
}

void Channel::commit(std::function<void (const bool)> cb, CChannel source) {
    Channel *source_ptr;
    if (source) {
        source_ptr = source.get();
    } else {
        source_ptr = this;
    }
    env.bot->call("PATCH", "/channels/"+std::to_string(id), source_ptr->dump(), [cb] (const bool error, nlohmann::json data) {
        if (not error) {
            cache::new_channel(data["body"]);
        }
        if (cb) cb(error);
    });
}

std::string Channel::get_mention() const {
    return "<#"+std::to_string(id)+">";
}

void Channel::send_json(nlohmann::json msg, std::function<void (CMessage)> cb) {
    if (_in_reply_to) {
        nlohmann::json ref_data = {
            {"message_id", _in_reply_to->id},
            {"channel_id", _in_reply_to->channel_id}
        };
        if (_in_reply_to->guild_id) {
            ref_data["guild_id"] = _in_reply_to->guild_id;
        }
        msg["message_reference"] = ref_data;
    }

    env.bot->call("POST", "/channels/" + std::to_string(id) + "/messages", msg, [cb] (const bool error, const nlohmann::json resp) {
        CMessage fres = nullptr;
        if (not error) {
            fres = cache::new_message(resp["body"]);
        }
        if (cb) cb(fres);
    });
}

void Channel::edit_message(const uint64_t message_id, const std::string& text, std::function<void (CMessage)> cb, const nlohmann::json& embed, uint32_t flags) {
    env.bot->call("PATCH", "/channels/" + std::to_string(id) + "/messages/"+std::to_string(message_id), nlohmann::json({
                                                                                   {"content", text},
                                                                                   {"embed", embed},
                                                                                   {"flags", flags}
                                                                               }), [cb] (const bool error, const nlohmann::json resp) {
        CMessage fres = nullptr;
        if (not error) {
            fres = cache::new_message(resp["body"]);
        }
        if (cb) cb(fres);
    });
}

bool Channel::has_perm(uint64_t uor_id, bool is_role, Permissions::type permission) const {
    // Check overwrites
    auto res = overwrites.find(uor_id);
    if (res != overwrites.end()) {
        auto &overwrite = res->second;
        if ((overwrite.deny & permission) == permission) {
            return false;
        } else if ((overwrite.allow & permission) == permission) {
            return true;
        }
    }
    // Check role permissions
    return get_guild()->has_perm(uor_id, is_role, permission);
}

void Channel::remove(std::function<void (const bool)> cb) {
    env.bot->call("DELETE", "/channels/"+std::to_string(id), [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}

void Channel::get_pins(std::function<void (const bool, const std::unordered_map<uint64_t, CMessage>&)> cb) {
    if (not _pins_cache) {
        _pins_cache = new std::unordered_map<uint64_t, CMessage>;
        env.bot->call("GET", "/channels/"+std::to_string(id)+"/pins", [this, cb] (const bool error, const nlohmann::json data) {
            if (error) cb(true, messages);
            for (const auto& [_, jmsg] : data["body"].items()) {
                CMessage msg;
                auto res = messages.find(s2i(jmsg["id"]));
                if (res != messages.end()) {
                    msg = res->second;
                } else {
                    msg = std::make_shared<Message>(jmsg);
                    messages[msg->id] = msg;
                }
                (*_pins_cache)[msg->id] = msg;
                cb(false, *_pins_cache);
            }
        });
    } else {
        cb(false, *_pins_cache);
    }
}

void Channel::pin_message(uint64_t msg_id, std::function<void (const bool)> cb) {
    env.bot->call("PUT", "/channels/"+std::to_string(id)+"/pins/"+std::to_string(msg_id), [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}

void Channel::unpin_message(uint64_t msg_id, std::function<void (const bool)> cb) {
    env.bot->call("DELETE", "/channels/"+std::to_string(id)+"/pins/"+std::to_string(msg_id), [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}

void Channel::pin_message(CMessage msg, std::function<void (const bool)> cb) {
    pin_message(msg->id, cb);
}
void Channel::unpin_message(CMessage msg, std::function<void (const bool)> cb) {
    unpin_message(msg->id, cb);
}

void Channel::create_invite(std::function<void (std::optional<Invite>)> cb, bool temporary, bool unique, uint32_t max_uses, uint32_t max_age) {
    env.bot->call("POST", "/channels/"+std::to_string(id)+"/invites", {
                      {"temporary", temporary},
                      {"unique", unique},
                      {"max_uses", max_uses},
                      {"max_age", max_age}
                  }, [cb] (const bool error, nlohmann::json data) {
        std::optional<Invite> fres = {};
        if (not error) {
            fres = Invite(data["body"]);
        }
        if (cb) cb(fres);
    });
}

void Channel::get_invites(std::function<void (std::optional<std::vector<Invite>>)> cb) {
    env.bot->call("GET", "/channels/"+std::to_string(id)+"/invites",
                  [cb] (const bool error, nlohmann::json data) {
        if (error) {
            cb(std::nullopt);
        } else {
            std::vector<Invite> fres;
            for (const auto& elem : data["body"].items()) {
                fres.push_back(Invite(elem));
            }
            cb(fres);
        }
    });
}

void Channel::start_typing(std::function<void (const bool)> cb) {
    env.bot->call("POST", "/channels/"+std::to_string(id)+"/typing",
                  [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}
};
