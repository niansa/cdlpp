#include <string>
#include <memory>
#include <nlohmann/json.hpp>
#include "abstract/reaction.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/json_f.hpp"
#include "cdltypes-incomplete.hpp"



namespace CDL {
Reaction::Reaction(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("user_id", user_id);
        JSON_FPARSE_ID("channel_id", channel_id);
        JSON_FPARSE_ID("message_id", message_id);
        JSON_FPARSE_ID("guild_id", guild_id);
        JSON_FPARSE("emoji", _emoji_raw);
    }
    if (data.contains("member") and data["member"].is_string()) {
        member = cache::new_member(data["member"], guild_id, user_id);
    }
}

void Reaction::get_emoji(std::function<void (CEmoji)> cb) {
    if (_emoji) {
        cb(_emoji);
    } else {
        get_guild([this, cb] (CGuild guild) {
            _emoji = cache::new_emoji(_emoji_raw, guild.get());
            cb(_emoji);
        });
    }
}
void Reaction::get_emoji(std::function<void (CEmoji)> cb) const {
    if (_emoji) {
        cb(_emoji);
    } else {
        get_guild([this, cb] (CGuild guild) {
            cb(cache::new_emoji(_emoji_raw, guild.get()));
        });
    }
}
}
