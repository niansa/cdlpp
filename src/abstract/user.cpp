#include <string>
#include <functional>
#include <nlohmann/json.hpp>
#include "abstract/user.hpp"
#include "abstract/guild.hpp"
#include "abstract/cache.hpp"
#include "abstract/env.hpp"
#include "abstract/presence.hpp"
#include "abstract/json_f.hpp"
#include "cdltypes-incomplete.hpp"
#include "extras.hpp"



namespace CDL {
nlohmann::json User::dump() {
    nlohmann::json res;
    JSON_FDUMP_BEGIN(res) {
        JSON_FDUMP("username", username);
        JSON_FDUMP("discriminator", std::to_string(discriminator));
        JSON_FDUMP("avatar", avatar);
        JSON_FDUMP("system", system);
        JSON_FDUMP("bot", bot);
        JSON_FDUMP("mfa_enabled", mfa_enabled);
    }
    return res;
}

void User::update(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("id", id);
        JSON_FPARSE("username", username);
        JSON_FPARSE_CUSTOM("discriminator", obj, {
                               discriminator = std::stoi(std::string(obj));
                           });
        JSON_FPARSE("avatar", avatar);
        JSON_FPARSE("system", system);
        JSON_FPARSE("bot", bot);
        JSON_FPARSE("mfa_enabled", mfa_enabled);
    }
}

CMember User::get_member(CGuild guild) const {
    // Get member
    auto res = guild->members.find(id);
    if (res == guild->members.end()) {
        return nullptr;
    }
    // Return result
    return res->second;
}

CMember User::get_member(uint64_t guild_id) const {
    // Get guild
    auto guild = cache::get_guild(guild_id);
    if (not guild) {
        return nullptr;
    }
    // Call second function
    return get_member(guild);
}

void User::get_dm(std::function<void (CChannel )> cb) {
    env.bot->call("POST", "/users/@me/channels", {{"recipient_id", id}}, [cb] (const bool error, const nlohmann::json data) {
        if (error) {
            cb(nullptr);
        } else {
            cb(cache::new_channel(data["body"]));
        }
    });
}
}
