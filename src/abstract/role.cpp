#include <cstdint>
#include <string>
#include <nlohmann/json.hpp>
#include "abstract/role.hpp"
#include "abstract/guild.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/permissions.hpp"
#include "abstract/env.hpp"
#include "abstract/json_f.hpp"
#include "cdltypes-incomplete.hpp"



namespace CDL {

void Role::update(const nlohmann::json& data, CGuild guild) {
    if (guild) {
        this->guild_id = guild->id;
    }
    this->lazy_guild = guild;
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("id", id);
        JSON_FPARSE("name", name);
        JSON_FPARSE("color", color);
        JSON_FPARSE("hoist", hoist);
        JSON_FPARSE("position", position);
        JSON_FPARSE("managed", managed);
        JSON_FPARSE("mentionable", mentionable);
        JSON_FPARSE_CUSTOM("permissions", permstr, {
                               permissions = static_cast<Permissions::type>(s2i(permstr));
                           });
        JSON_FPARSE_CUSTOM("tags", obj, {
                               JSON_FPARSE_BEGIN(obj) {
                                   JSON_FPARSE_ID("bot_id", tags.bot_id);
                                   JSON_FPARSE_ID("integration_id", tags.integration_id);
                                   JSON_FPARSE("premium_subscriber", tags.premium_subscriber);
                               }
                           });
    }
}

nlohmann::json Role::dump(bool position) const {
    nlohmann::json res;
    JSON_FDUMP_BEGIN(res) {
        JSON_FDUMP_ID("permissions", permissions);
        JSON_FDUMP("name", name);
        JSON_FDUMP("color", color);
        JSON_FDUMP("hoist", hoist);
        JSON_FDUMP("mentionable", mentionable);
    }
    if (position) {
        res["position"] = this->position;
    }
    return res;
}
nlohmann::json RoleUpdate::dump() const {
    nlohmann::json res;
    JSON_FDUMP_BEGIN(res) {
        JSON_FDUMP_NNULL(permissions)
        JSON_FDUMP("permissions", std::to_string(permissions.value()));
        JSON_FDUMP_NNULL(name)
        JSON_FDUMP("name", name.value());
        JSON_FDUMP_NNULL(color)
        JSON_FDUMP("color", color.value());
        JSON_FDUMP_NNULL(hoist)
        JSON_FDUMP("hoist", hoist.value());
        JSON_FDUMP_NNULL(mentionable)
        JSON_FDUMP("mentionable", mentionable.value());
    }
    return res;
}

void Role::commit(std::function<void (const bool)> cb, const Role *source) {
    if (not source) source = this;
    env.bot->call("PATCH", "/guilds/"+std::to_string(guild_id)+"/roles/"+std::to_string(id), source->dump(), [this, cb] (const bool error, nlohmann::json data) {
        if (not error) {
            this->update(data["body"], lazy_guild);
        }
        if (cb) cb(error);
    });
}
void Role::commit(std::function<void (const bool)> cb, const RoleUpdate& source) {
    env.bot->call("PATCH", "/guilds/"+std::to_string(guild_id)+"/roles/"+std::to_string(id), source.dump(), [this, cb] (const bool error, nlohmann::json data) {
        if (not error) {
            this->update(data["body"], lazy_guild);
        }
        if (cb) cb(error);
    });
}

void Role::get_guild(std::function<void (CGuild)> cb) {
    if (lazy_guild) {
        cb(lazy_guild);
    } else {
        fetch::guild(guild_id, [this, cb] (CGuild guild) {
            lazy_guild = guild;
            cb(guild);
        });
    }
}
void Role::get_guild(std::function<void (CGuild)> cb) const {
    if (lazy_guild) {
        cb(lazy_guild);
    } else {
        fetch::guild(guild_id, [cb] (CGuild guild) {
            cb(guild);
        });
    }
}

void Role::remove(std::function<void (const bool)> cb) {
    get_guild([this, cb] (CGuild server) {
        if (not server) {
            cb(false);
        } else {
            server->remove_role(id, cb);
        }
    });
}
}
