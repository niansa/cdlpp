#include <functional>
#include <nlohmann/json.hpp>
#include "bot.hpp"
#include "abstract/message.hpp"
#include "abstract/channel.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/env.hpp"
#include "abstract/guild.hpp"
#include "cdltypes-incomplete.hpp"

#define try_cache(id, type) {type *res; if ((res = cache::fetch<type>(id))) {return res;}}0

namespace CDL {
namespace fetch {
void user(uint64_t id, std::function<void (CUser)> cb) {
    auto res = cache::get_user(id);
    if (res) return cb(res);
    // Try fetching from
    env.bot->call("GET", "/users/"+std::to_string(id), [cb] (const bool error, const nlohmann::json resp) {
        if (error) {
            cb(nullptr);
        } else {
            cb(cache::new_user(resp["body"]));
        }
    });
}

void message(uint64_t id, std::function<void (CMessage)> cb, const_CChannel channel) {
    auto res = channel->messages.find(id);
    if (res != channel->messages.end()) return cb(res->second);
    // Try fetching
    env.bot->call("GET", "/channels/"+std::to_string(id)+"/messages/"+std::to_string(id), [cb] (const bool error, const nlohmann::json resp) {
        if (error) {
            cb(nullptr);
        } else {
            cb(cache::new_message(resp["body"]));
        }
    });
}

void channel(uint64_t id, std::function<void (CChannel)> cb, const_CGuild guild) {
    CChannel res = nullptr;
    if (guild) {
        auto res = guild->channels.find(id);
        if (res != guild->channels.end()) {
            cb(res->second);
            return;
        }
    } else {
        res = cache::get_channel(id);
        if (res) {
            cb(res);
            return;
        }
    }
    // Try fetching
    env.bot->call("GET", "/channels/"+std::to_string(id), [cb] (const bool error, const nlohmann::json resp) {
        if (error) {
            cb(nullptr);
        } else {
            cb(cache::new_channel(resp["body"]));
        }
    });
}

void guild(uint64_t id, std::function<void (CGuild)> cb) {
    auto res = cache::get_guild(id);
    if (res) return cb(res);
    // Try fetching
    env.bot->call("GET", "/guilds/"+std::to_string(id), [cb] (const bool error, const nlohmann::json resp) {
        if (error) {
            cb(nullptr);
        } else {
            cb(cache::new_guild(resp["body"]));
        }
    });
}
}
};
