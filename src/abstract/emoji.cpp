#include <string>
#include <nlohmann/json.hpp>
#include "abstract/emoji.hpp"
#include "abstract/guild.hpp"
#include "abstract/permissions.hpp"
#include "abstract/cache.hpp"
#include "abstract/json_f.hpp"
#include "extras.hpp"
#include "cdltypes-incomplete.hpp"


namespace CDL {
void Emoji::update(const nlohmann::json& data, Guild *guild) {
    this->guild = guild;
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("id", id);
        JSON_FPARSE("name", name);
        JSON_FPARSE("require_colons", require_colons);
        JSON_FPARSE("managed", managed);
        JSON_FPARSE("animated", animated);
        JSON_FPARSE("available", available);
        JSON_FPARSE_VFNC("user", user, cache::new_user);
        JSON_FPARSE_CUSTOM("roles", jroles, {
                               if (guild) {
                                   for (const auto& [_, role_id] : jroles.items()) {
                                       auto res = guild->roles.find(s2i(role_id));
                                       if (res != guild->roles.end()) {
                                           roles.push_back(res->second);
                                       }
                                   }
                               }
                           });
    }
    custom = guild;
}
};
