cmake_minimum_required(VERSION 3.14)
cmake_policy(SET CMP0077 NEW)

project(cdlpp
    LANGUAGES CXX
    HOMEPAGE_URL https://gitlab.com/niansa/cdlpp
    DESCRIPTION "High level Discord library in C++ (based on Discord++)"
    VERSION 1.0
)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


option(CDLPP_BUILD_STATIC_LIB "Build a static library of CDL++ instead of a shared one" NO)
if (CDLPP_BUILD_STATIC_LIB)
    set(BUILD_TYPE "STATIC")
else()
    set(BUILD_TYPE "SHARED")
endif()

add_library(${PROJECT_NAME} ${BUILD_TYPE}
    src/bot.cpp
    src/dpp-wrap.cpp
    src/extras.cpp

    src/abstract/intents.cpp
    src/abstract/fetch.cpp
    src/abstract/cache.cpp
    src/abstract/env.cpp
    src/abstract/channel.cpp
    src/abstract/guild.cpp
    src/abstract/message.cpp
    src/abstract/role.cpp
    src/abstract/user.cpp
    src/abstract/member.cpp
    src/abstract/emoji.cpp
    src/abstract/presence.cpp
    src/abstract/reaction.cpp
)

target_include_directories(${PROJECT_NAME} PRIVATE
    ${discordpp_SOURCE_DIR}
    ${discordpp-rest-beast_SOURCE_DIR}
    ${discordpp-websocket-simpleweb_SOURCE_DIR}
    ${discordpp-plugin-overload_SOURCE_DIR}
    ${discordpp-plugin-ratelimit_SOURCE_DIR}
)

target_include_directories(${PROJECT_NAME} PUBLIC
    include/
)

find_package(PkgConfig REQUIRED)
pkg_check_modules(format REQUIRED IMPORTED_TARGET fmt)

target_link_libraries(${PROJECT_NAME} PUBLIC
        PkgConfig::format)
target_link_libraries(${PROJECT_NAME} PUBLIC
        "crypto")
target_link_libraries(${PROJECT_NAME} PUBLIC
        "ssl")
target_link_libraries(${PROJECT_NAME} PUBLIC
        "pthread")
target_link_libraries(${PROJECT_NAME} PUBLIC
        "stdc++fs")

target_compile_definitions(${PROJECT_NAME} PRIVATE CDLPP_LIBRARY)

configure_file(cdlpp.pc.in cdlpp.pc @ONLY)

install(TARGETS ${PROJECT_NAME} LIBRARY)
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/cdlpp.pc" DESTINATION ${CMAKE_INSTALL_PREFIX}/../share/pkgconfig)
install(DIRECTORY "${CMAKE_SOURCE_DIR}/include/"
        DESTINATION "include/cdlpp"
        FILES_MATCHING PATTERN "*.hpp"
)



add_subdirectory(lib/discordpp)
add_subdirectory(lib/rest-beast)
add_subdirectory(lib/websocket-simpleweb)
add_subdirectory(lib/plugin-overload)
add_subdirectory(lib/plugin-ratelimit)
